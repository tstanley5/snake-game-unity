This is my project in unity 2D of a snake game. I make it for my
service learning project that involves object oriented programming

In this project are the stuff I used to make the game in Unity 2D
To control the snake, you can use the arrow keys or the WASD keys. I think the time settings should make the game playable when I checked so it shouldn't be too fast.

When the snake eats the food, it will grow a little. Over time,
the snake will become extra long! However, watch out for the walls and the snake's body or it's back to zero!

The snake will not move at the start so reaction time should be no problem.

It's not very detailed imagewise due to me not being able to
render detailed sprites without the details being lost to artifacting. So for now, I'm using simple squares and rectanges
to make up the game just the good old days, am I right?

Feel free to put this project on your own machine to try it out. I didn't make up the concept of the snake game so I consider this project to be open source.