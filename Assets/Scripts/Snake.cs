// This class controls the behavior of the snake and its segments
// Also displays score text onscreen

using System.Collections.Generic; // for generics
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public class Snake : MonoBehaviour
{
    // the vector has two float point values of x and y
    private Vector2 _direction = Vector2.zero; // snake will not move at start
    private List<Transform> _segments = new List<Transform>(); // uses a generic, and this is a ref to the segments in generic form

    // These will display in editor
    public Transform segmentPrefab; // makes a reference to a segment prefab
    public int initialSize = 4; // this shows up in editor
    [SerializeField] TextMeshProUGUI ScoreText;
    [SerializeField] TextMeshProUGUI ScoreNumber;

    // Variable to keep tract of score
    int scorenum;

    private void Start()
    {
        StartingState();
    }

    private void Update()
    {
        ScoreText.color = Color.black;
        ScoreText.text = "Score:";
        ScoreNumber.color = Color.black;
        ScoreNumber.text = scorenum.ToString();

        bool changed_direction = false;

        if (changed_direction == false)
        {
            // The changed direction boolean will prevent the snake from
            // going in the opposite direction so that the snake's head
            // cannot collide with its segments when the opposite direction key is pressed

            // Key inputs influence the direction that the snake travels
            if (Input.GetKeyDown(KeyCode.W) && _direction != Vector2.down)
            {
                _direction = Vector2.up; // move up with W key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow) && _direction != Vector2.down)
            {
                _direction = Vector2.up; // move up with up arrow key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.S) && _direction != Vector2.up)
            {
                _direction = Vector2.down; // move down with S key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && _direction != Vector2.up)
            {
                _direction = Vector2.down; // move down with down arrow key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.A) && _direction != Vector2.right)
            {
                _direction = Vector2.left; // move left with A key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) && _direction != Vector2.right)
            {
                _direction = Vector2.left; // move left with left arrow key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.D) && _direction != Vector2.left)
            {
                _direction = Vector2.right; // move right with D key
                changed_direction = true;
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) && _direction != Vector2.left)
            {
                _direction = Vector2.right; // move right with right arrow key
                changed_direction = true;
            }
        }
    } // end of update

    private void FixedUpdate()
    {
        for (int i = _segments.Count - 1; i > 0; i--)
        {
            _segments[i].position = _segments[i - 1].position;
        }
        // access the transform object of the snake
        // Mathf.Round will allow the numbers to round up to whole numbers to 
        // allow the snake to follow the grid
        this.transform.position = new Vector3(
            Mathf.Round(this.transform.position.x) + _direction.x,
            Mathf.Round(this.transform.position.y) + _direction.y,
            0.0f // the Z component is set to zero
        );
    }

    private void Grow()
    {
        scorenum++;
        // when the snake eats a food, it will grow a segment
        // on the tail end like a queue of segments
        Transform segment = Instantiate(this.segmentPrefab);
        segment.position = _segments[_segments.Count - 1].position;

        _segments.Add(segment);
    }

    private void StartingState()
    {
        scorenum = 0;
        // Let's reset the game back to starting state

        for (int i = 1; i < _segments.Count; i++)
        {
            // remove the segment prefabs
            Destroy(_segments[i].gameObject);
        }

        _segments.Clear();
        _segments.Add(this.transform);

        // I want to make the snake not move when it's reset
        // so that the snake doesn't crash into a wall over a over
        // from each reset cycle
        _direction = Vector2.zero;

        for (int i = 1; i < this.initialSize; i++)
        {
            _segments.Add(Instantiate(this.segmentPrefab));
        }

        // reset snake's position to zero
        this.transform.position = Vector3.zero;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // handles an event when the snake head collides with an object

        if(other.tag == "Food")
        {
            // if the snake touches the food, make it grow a segment
            Grow();
        }
        else if (other.tag == "Obstacle")
        {
            // The walls and the snake's segments are treated as obstacles
            StartingState();
        }
    
    
    }
}
