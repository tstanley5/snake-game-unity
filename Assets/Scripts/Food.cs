// This class controls the behavior of the food object
using UnityEngine;

public class Food : MonoBehaviour
{
    // create a reference to the grid area
    public BoxCollider2D gridArea;

    private void Start()
    {
        // have the food spawn at a random position
        RandomizePosition();
    }

    private void RandomizePosition()
    {
        Bounds bounds = this.gridArea.bounds;
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float y = Random.Range(bounds.min.y, bounds.max.y);

        // Random.Range returns float values so mathf will need to make
        // the number whole for the grid
        this.transform.position = new Vector3(Mathf.Round(x), Mathf.Round(y), 0.0f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Snake has the Player Tag, so the food's collision can find it specifically
        if (other.tag == "Player")
        {
            RandomizePosition();
        }
            
    }
}
